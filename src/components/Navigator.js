import React from 'react';
import Button from 'react-bootstrap/Button';

const buttonArray = [
    {
        text: 'Добавить книгу',
        title: 'newbook'
    },
    {
        text: 'Книги',
        title: 'book'
    },
    {
        text: 'Добавить автора ',
        title: 'newauthor'
    },
    {
        text: 'Авторы',
        title: 'author'
    },
    {
        text: 'Добавить жанр',
        title: 'newgenre'
    },
    {
        text: 'Жанры',
        title: 'genre'
    }
]

class Navigator extends React.Component{
    render(){
        const {changeWindow} = this.props;
        return (
            <div className='navigator'>
                {buttonArray.map((button) => (
                    <div className="navigator__button">
                        <Button  variant="primary" onClick={changeWindow.bind(this, button.title)}>{button.text}</Button >
                    </div>
                ))}
            </div>
        )
    }
}

export default Navigator;
