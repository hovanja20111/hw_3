import React from 'react';

const GENRE_URL = "http://hovanja2012.pythonanywhere.com/api/book/"

class NewGenre extends React.Component{
    state = {
        create_success: undefined,
    }

    regBook = async () => {
        const {token} = this.props;
        let title = this.titleRef.value;
        let name = this.nameRef.value;
        try{
            const result = await fetch(GENRE_URL, {
                method: "POST",
                headers: { 'Content-Type': 'application/json',
                'Authorization': 'Token ' + token },
                body: JSON.stringify({
                    'title': title,
                    'name': name,
                })
            })
            let tmp = await result.json();
            if ('id' in tmp){
                this.setState({create_success: true})
            }
            else{
                console.log(tmp);
                this.setState({create_success: false})
            }

        }catch (err){
            this.setState({
                error: "Ошибка получения данных"
            })
        }
    }


    render(){
        let {create_success} = this.state;
        return (
            <div className='leftmodal'>
                <div><h1>Добавить жанр</h1></div>
                <div>Имя: <input type="text" name="name" size="30" maxLength="50" ref={ref => this.titleRef = ref} /></div>
                <div>Название: <input type="text" name="department" size="30" maxLength="100" ref={ref => this.nameRef = ref} /></div>                
                <button onClick={this.regBook}>Создать</button>
                {create_success? <div><h3 className='success'>Жанр добавлен!</h3></div>:
                    <div></div>
                }
                
            </div>
        )
    }
}

export default NewGenre;
