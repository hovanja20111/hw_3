import './App.css';
import React from 'react';
import LoginForm from './components/LoginForm'
import Navigator from './components/Navigator'
import Header from './components/header';
import CookiesManager from 'js-cookie';
import Author from './components/Tables/Author';
import NewAuthor from './components/Tables/NewAuthor';
import Book from './components/Tables/Book';
import NewBook from './components/Tables/NewBook';
import Genre from './components/Tables/Genre';
import NewGenre from './components/Tables/NewGenre';

class App extends React.Component {
  state = {
    isLoginned: false,
    token: undefined,
    name: "Хованская Аня",
    activeWindow: 'book',
    viewId: undefined,
  }

  hadleLoginClick = () => {
    if (this.state.isLoginned === false) {
      this.setState({ isLoginned: true })
      CookiesManager.set('isLoginned', true);
    }
    else {
      CookiesManager.set('isLoginned', false);
      CookiesManager.set('token', undefined);
      this.setState({ isLoginned: false })
    }
  }

  changeWindow = (new_window) => {
    this.setState({ activeWindow: new_window })
  }

  setToken = (s_token) => {
    this.setState({ token: s_token })
    CookiesManager.set('token', s_token);
  }

  setView = (activeWindow, id) => {
    this.setState({ activeWindow: activeWindow, viewId: id })
  }

  render() {
    return (
      <>
        {this.state.isLoginned === true ? <div >
          <Header onButtonClick={this.hadleLoginClick} name={this.state.name} />
          <div key='mainscreen' className='MainScreen'>
            <Navigator changeWindow={this.changeWindow} />
            {(() => {
              switch (this.state.activeWindow) {
                case 'author':
                  return (
                    <Author token={this.state.token} setView={this.setView} />
                  )
                case 'newauthor':
                  return (
                    <NewAuthor token={this.state.token} setView={this.setView} />
                  )
                case 'book':
                  return (
                    <Book token={this.state.token} setView={this.setView} />
                  )
                case 'newbook':
                  return (
                    <NewBook token={this.state.token} setView={this.setView} />
                  )
                case 'genre':
                  return (
                    <Genre token={this.state.token} setView={this.setView} />
                  )
                case 'newgenre':
                  return (
                    <NewGenre token={this.state.token} setView={this.setView} />
                  )
              }

            })()}
          </div>
          chfvjgbkunhlim;
        </div> : <LoginForm onButtonClick={this.hadleLoginClick} setToken={this.setToken} isLoginned={this.state.isLoginned} token={this.state.token} />}
      </>
    );
  }
}

export default App;
